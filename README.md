# Lunch Picker Spring Boot Application

The Lunch Picker Spring Boot application is a simple RESTful API for coordinating lunch decisions within a group. It allows users to initiate a lunch session, join sessions, submit restaurant choices, and randomly pick a restaurant at the end of the session.

## Prerequisites

- Java 8 or later
- Maven

## Getting Started

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/mshanmugamjj/LunchPicker.git


2. API Endpoints:

   a) Initiate a Session

    - Endpoint: /session/initiate

    - Method: POST
    
    - Description: Initiates a new lunch session.
    
    - Example:
   
    ```bash
   curl -X POST http://localhost:8080/session/initiate
   ```

   b) Join a Session

   - Endpoint: /session/join/{sessionId}

   - Method: POST

   - Description: Joins an existing lunch session.
   
   - Parameters:

      - {sessionId}: The ID of the session to join.

   - Example:

    ```bash
   curl -X POST http://localhost:8080/session/join/{sessionId}
   ```

   c) Submit a Restaurant

   - Endpoint: /session/submit/{sessionId}

   - Method: POST

   - Description: Submits a restaurant choice for the current session.

   - Parameters:

      - {sessionId}: The ID of the session to submit the restaurant to.

   - Request Body:
   
   ```bash
   {
      "name": "Restaurant Name"
   }
   ```

   - Example:

   ```bash
   curl -X POST -H "Content-Type: application/json" -d '{"name":"Restaurant Name"}' http://localhost:8080/session/submit/{sessionId}
   ```

   d) Get Restaurants

   - Endpoint: /session/restaurants/{sessionId}

   - Method: GET

   - Description: Retrieves the list of submitted restaurants for the current session.

   - Parameters:

      - {sessionId}: The ID of the session to get restaurants from.

   - Example:

    ```bash
   curl http://localhost:8080/session/restaurants/{sessionId}
   ```

   e) End a Session

   - Endpoint: /session/end/{sessionId}

   - Method: POST

   - Description: Ends the current session and randomly picks a restaurant.

   - Parameters:

      - {sessionId}: The ID of the session to end.

   - Example:

    ```bash
   curl -X POST http://localhost:8080/session/end/{sessionId}
   ```