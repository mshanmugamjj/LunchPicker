package com.example.LunchPicker.service;

import com.example.LunchPicker.model.Restaurant;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.List;

public class LunchSessionServiceTest {

    @Test
    public void testInitiateSession() {
        LunchSessionService service = new LunchSessionService();
        String sessionId = service.initiateSession();
        assertNotNull(sessionId);
    }

    @Test
    public void testJoinSession() {
        LunchSessionService service = new LunchSessionService();
        String sessionId = service.initiateSession();
        String result = service.joinSession(sessionId);
        assertEquals("User joined the session: " + sessionId, result);
    }

    @Test
    public void testSubmitRestaurant() {
        LunchSessionService service = new LunchSessionService();
        String sessionId = service.initiateSession();
        Restaurant restaurant = new Restaurant("Test Restaurant");
        service.submitRestaurant(sessionId, restaurant);
        List<Restaurant> restaurants = service.getRestaurants(sessionId);
        assertEquals(1, restaurants.size());
        assertEquals("Test Restaurant", restaurants.get(0).getName());
    }

    @Test
    public void testEndSession() {
        LunchSessionService service = new LunchSessionService();
        String sessionId = service.initiateSession();
        Restaurant restaurant = new Restaurant("Test Restaurant");
        service.submitRestaurant(sessionId, restaurant);
        Restaurant pickedRestaurant = service.endSession(sessionId);
        assertNotNull(pickedRestaurant);
        assertTrue(service.getRestaurants(sessionId).isEmpty());
    }
}
