package com.example.LunchPicker.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class RestaurantTest {

    @Test
    public void testGetSetName() {
        Restaurant restaurant = new Restaurant();
        restaurant.setName("Test Restaurant");
        assertEquals("Test Restaurant", restaurant.getName());
    }
}
