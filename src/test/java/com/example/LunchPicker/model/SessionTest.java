package com.example.LunchPicker.model;

import org.junit.jupiter.api.Test;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class SessionTest {

    @Test
    public void testInitiateSession() {
        Session session = new Session();
        assertNotNull(session.getSessionId());
        assertFalse(session.isEnded());
        assertTrue(session.getRestaurants().isEmpty());
    }

    @Test
    public void testAddRestaurant() {
        Session session = new Session();
        Restaurant restaurant = new Restaurant("Test Restaurant");
        session.getRestaurants().add(restaurant);
        assertEquals(1, session.getRestaurants().size());
        assertEquals("Test Restaurant", session.getRestaurants().get(0).getName());
    }
}
