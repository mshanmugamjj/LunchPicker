package com.example.LunchPicker.controller;

import com.example.LunchPicker.model.Restaurant;
import com.example.LunchPicker.model.Session;
import com.example.LunchPicker.service.LunchSessionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/session")
public class LunchSessionController {

    private final LunchSessionService lunchSessionService;

    public LunchSessionController(LunchSessionService lunchSessionService) {
        this.lunchSessionService = lunchSessionService;
    }

    @PostMapping("/initiate")
    public String initiateSession() {
        return lunchSessionService.initiateSession();
    }

    @PostMapping("/join/{sessionId}")
    public String joinSession(@PathVariable String sessionId) {
        return lunchSessionService.joinSession(sessionId);
    }

    @PostMapping("/submit/{sessionId}")
    public void submitRestaurant(@PathVariable String sessionId, @RequestBody Restaurant restaurant) {
        lunchSessionService.submitRestaurant(sessionId, restaurant);
    }

    @GetMapping("/restaurants/{sessionId}")
    public List<Restaurant> getRestaurants(@PathVariable String sessionId) {
        return lunchSessionService.getRestaurants(sessionId);
    }

    @PostMapping("/end/{sessionId}")
    public Restaurant endSession(@PathVariable String sessionId) {
        return lunchSessionService.endSession(sessionId);
    }
}
