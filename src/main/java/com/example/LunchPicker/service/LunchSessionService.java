package com.example.LunchPicker.service;

import com.example.LunchPicker.model.Restaurant;
import com.example.LunchPicker.model.Session;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Service
public class LunchSessionService {

    private final List<Session> sessions = new ArrayList<>();

    public String initiateSession() {
        Session session = new Session();
        sessions.add(session);
        return session.getSessionId();
    }

    public String joinSession(String sessionId) {
        Session session = findSession(sessionId);
        if (!session.isEnded()) {
            return "User joined the session: " + sessionId;
        } else {
            return "Cannot join an ended session: " + sessionId;
        }
    }

    public void submitRestaurant(String sessionId, Restaurant restaurant) {
        Session session = findSession(sessionId);
        if (!session.isEnded()) {
            session.getRestaurants().add(restaurant);
        }
    }

    public List<Restaurant> getRestaurants(String sessionId) {
        Session session = findSession(sessionId);
        if (!session.isEnded()) {
            return session.getRestaurants();
        } else {
            return Collections.emptyList();
        }
    }

    public Restaurant endSession(String sessionId) {
        Session session = findSession(sessionId);
        session.setEnded(true);

        List<Restaurant> restaurants = session.getRestaurants();
        if (restaurants.isEmpty()) {
            return null;
        }

        // Randomly pick a restaurant
        Random random = new Random();
        int index = random.nextInt(restaurants.size());
        return restaurants.get(index);
    }

    private Session findSession(String sessionId) {
        return sessions.stream()
                .filter(session -> session.getSessionId().equals(sessionId))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Session not found: " + sessionId));
    }
}
