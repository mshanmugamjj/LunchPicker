package com.example.LunchPicker.model;

import java.util.ArrayList;
import java.util.List;

public class Session {
    private String sessionId;
    private boolean ended;
    private List<Restaurant> restaurants;

    public Session() {
        this.sessionId = generateSessionId();
        this.restaurants = new ArrayList<>();
    }

    public String getSessionId() {
        return sessionId;
    }

    public boolean isEnded() {
        return ended;
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
    }

    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    private String generateSessionId() {
        return Integer.toHexString(this.hashCode());
    }
}
